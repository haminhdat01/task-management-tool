# Task Management Tool


###TaskManagement

var taskList = [];<br>
var taskData = [];<br>

const **runProcess** = (job) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Process or run job.<br>
}

const **calculateDelayTime** = (cronJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// **cronJob** use schedule expressions to standard format. You can via link https://crontab.guru/ <br>
&nbsp;&nbsp;&nbsp;&nbsp;return **delayTime**: Number;<br>
}

const **cronTab** = (doJob, cronJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;var sleepTime = **calculateDelayTime**(cronJob); // Get delay time<br>
&nbsp;&nbsp;&nbsp;&nbsp;var waitTime = sleepTime;<br>
&nbsp;&nbsp;&nbsp;&nbsp;var startTime = Date.now(); // Get current time<br>

&nbsp;&nbsp;&nbsp;&nbsp;runProcess(doJob);<br>

&nbsp;&nbsp;&nbsp;&nbsp;var currentTime = Date.now();<br>
&nbsp;&nbsp;&nbsp;&nbsp;waitTime = sleepTime - (currentTime - startTime);<br>

&nbsp;&nbsp;&nbsp;&nbsp;setTimeout(() => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cronTab();<br>
&nbsp;&nbsp;&nbsp;&nbsp;}, waitTime);<br>
}

// Implement according to PM2 command line.<br>
export const **add** = (doJob, cronJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Add new **doJob** with scheduled time (cronJob).<br>
&nbsp;&nbsp;&nbsp;&nbsp;taskList.add({doJob, cronJob});<br>
}<br>

export const **start** = (jobIndex, doJob, cronJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Add and start new **doJob** with scheduled time (cronJob).<br>
&nbsp;&nbsp;&nbsp;&nbsp;**add**(doJob, cronJob);<br>
&nbsp;&nbsp;&nbsp;&nbsp;if (jobIndex && !doJob && !cronJob) {<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;doJob = taskList[jobIndex].doJob;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cronJob = taskList[jobIndex].cronJob;<br>
&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;cronTab(doJob, cronJob);<br>
}<br>

export const **stop** = (jobIndex, doJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Stop doJob.<br>
}<br>

export const **pause** = (jobIndex, doJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Pause doJob.<br>
}<br>

export const **remove** = (jobIndex, doJob) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Remove doJob.<br>
}<br>

export const **export** = (jobIndex, data) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Export data of task. In this case is Task B.<br>
&nbsp;&nbsp;&nbsp;&nbsp;taskData[jobIndex] = data;<br>
}<br>

export const **import** = (jobIndex) => {<br>
&nbsp;&nbsp;&nbsp;&nbsp;// Import data of task. In this case is Task A can access data of Task B if it have demand.<br>
&nbsp;&nbsp;&nbsp;&nbsp;return taskData[jobIndex];<br>
}<br>

